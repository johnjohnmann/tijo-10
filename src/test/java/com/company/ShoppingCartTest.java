package com.company;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShoppingCartTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    public void init() {
        shoppingCart = new ShoppingCart();
        shoppingCart.addProduct(new Product("Banana", 300), 5);
        shoppingCart.addProduct(new Product("Pizza", 1000), 10);
        shoppingCart.addProduct(new Product("Car", 500000), 1);
    }

    @Test
    public void addProductTest() {
        assertEquals(3, shoppingCart.addProduct(new Product("Banana", 300), 5), "Adding a product DOESN'T work");
        assertEquals(4, shoppingCart.addProduct(new Product("Pizza", 1000), 10), "Adding a product DOESN'T work");
        assertEquals(5, shoppingCart.addProduct(new Product("Car", 500000), 1), "Adding a product DOESN'T work");
        assertEquals(-1, shoppingCart.addProduct(new Product("Product", 500), -500), "Adding products with a negative amount DOES work");
        assertEquals(-1, shoppingCart.addProduct(new Product("Product2", -100), 2), "Adding products with a negative price DOES work");
    }

    @Test
    public void deleteProductTest() {
        assertEquals(1, shoppingCart.deleteProduct(0, 4), "Deleting a product DOESN'T work");
        assertEquals(0, shoppingCart.deleteProduct(1, 10), "Deleting a product DOESN'T work");
        assertEquals(-1, shoppingCart.deleteProduct(2, 5), "Deleting a product DOESN'T work");
    }

    @Test
    public void countProductsTest() {
        assertEquals(16, shoppingCart.countProducts(), "Counting the products DOESN'T work");
    }

    @Test
    public void getProductPrices() {
        assertEquals(300, shoppingCart.getProductPrice(0), "Getting a product price DOESN'T work");
        assertEquals(500000, shoppingCart.getProductPrice(2), "Getting a product price DOESN'T work");
    }

    @Test
    public void getOverallPrice() {
        assertEquals(511500, shoppingCart.getOverallPrice(), "Getting the overall price DOESN'T work");
    }
}
