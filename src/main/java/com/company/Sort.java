package com.company;

import java.util.Arrays;

public class Sort {
    public int[] sortArray(int[] digits) {
        int[] tmpArray = Arrays.copyOf(digits, digits.length);
        Arrays.sort(tmpArray);
        return tmpArray;
    }
}