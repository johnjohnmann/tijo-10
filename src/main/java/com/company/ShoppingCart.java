package com.company;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private final List<ShoppingCartEntry> shoppingCart;
    private int id = 0;

    public ShoppingCart() {
        shoppingCart = new ArrayList<>();
    }

    public int addProduct(Product product, int amount) {
        if (amount < 1 || product.getPrice() < 0) {
            return -1;
        }
        shoppingCart.add(new ShoppingCartEntry(product, amount, id));
        return id++;
    }

    public int deleteProduct(int idToDelete, int amountToDelete) {
        for (int i = 0; i < shoppingCart.size(); i++) {
            ShoppingCartEntry entry = shoppingCart.get(i);
            if (entry.getId() == idToDelete) {
                if (entry.getAmount() < amountToDelete) {
                    return -1;
                } else if (entry.getAmount() == amountToDelete) {
                    shoppingCart.remove(i);
                    return 0;
                }
                entry.setAmount(entry.getAmount() - amountToDelete);
                return entry.getAmount();
            }
        }
        return -1;
    }

    public int countProducts() {
        int productCount = 0;
        for (ShoppingCartEntry entry : shoppingCart) {
            productCount += entry.getAmount();
        }
        return productCount;
    }

    public int getProductPrice(int id) {
        for (ShoppingCartEntry entry : shoppingCart) {
            if (entry.getId() == id) {
                return entry.getProduct().getPrice();
            }
        }
        return -1;
    }

    public int getOverallPrice() {
        int overallPrice = 0;
        for (ShoppingCartEntry entry : shoppingCart) {
            overallPrice += entry.getProduct().getPrice() * entry.getAmount();
        }
        return overallPrice;
    }
}
