package com.company;

public class ShoppingCartEntry {
    private Product product;
    private int amount;
    private int id;

    public ShoppingCartEntry(Product product, int amount, int id) {
        this.product = product;
        this.amount = amount;
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
